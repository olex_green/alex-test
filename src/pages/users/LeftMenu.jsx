import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import GradeIcon from '@material-ui/icons/Grade';
import GroupIcon from '@material-ui/icons/Group';
import InboxIcon from '@material-ui/icons/Inbox';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';

const styles = {
  root: {
    backgroundColor: 'white',
    marginTop: 2,
  },
  menuItemSelected: {
    backgroundColor: '#f5f5f5 !important',
  },
};

function LeftMenu({
  classes,
}) {
  return (
    <div className={classes.root}>
      <MenuList>
        <MenuItem key="1" selected classes={{ selected: classes.menuItemSelected }}>
          <ListItemIcon className={classes.icon}>
            <AccountCircleIcon />
          </ListItemIcon>
          <ListItemText inset primary="All Users" />
        </MenuItem>
        <MenuItem key="2" classes={{ selected: classes.menuItemSelected }}>
          <ListItemIcon className={classes.icon}>
            <GradeIcon />
          </ListItemIcon>
          <ListItemText inset primary="Favorites" />
        </MenuItem>
        <MenuItem key="3" classes={{ selected: classes.menuItemSelected }}>
          <ListItemIcon className={classes.icon}>
            <VerifiedUserIcon />
          </ListItemIcon>
          <ListItemText inset primary="Administrators" />
        </MenuItem>
        <MenuItem key="4" classes={{ selected: classes.menuItemSelected }}>
          <ListItemIcon className={classes.icon}>
            <GroupIcon />
          </ListItemIcon>
          <ListItemText inset primary="Non-Admins" />
        </MenuItem>
        <MenuItem key="5" classes={{ selected: classes.menuItemSelected }}>
          <ListItemIcon className={classes.icon}>
            <InboxIcon />
          </ListItemIcon>
          <ListItemText inset primary="Archived" />
        </MenuItem>
      </MenuList>
    </div>
  );
}

LeftMenu.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
    menuItemSelected: PropTypes.string,
  }),
};

LeftMenu.defaultProps = {
  classes: {},
};

export default withStyles(styles)(LeftMenu);
