import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';
import List from '@material-ui/core/List';

import { Search } from './Search';
import SearchItem from './SearchItem';

function setup(specProps) {
  const defaultProps = {
    classes: {},
    onSearchTextChange: () => {},
  };

  const props = {
    ...defaultProps,
    ...specProps,
  };
  const enzymeWrapper = shallow(<Search {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('Search', () => {
  describe('Rendering', () => {
    it('should render self and has first div', () => {
      const { enzymeWrapper } = setup();
      expect(enzymeWrapper.first().is(Paper)).to.equal(true);
    });

    it('should have single TextField', () => {
      const { enzymeWrapper } = setup();

      expect(enzymeWrapper.find(TextField).length).to.equal(1);
    });

    it('should have single List', () => {
      const { enzymeWrapper } = setup();

      expect(enzymeWrapper.find(List).length).to.equal(1);
    });

    it('by default. Should not have SearchItem', () => {
      const { enzymeWrapper } = setup();

      expect(enzymeWrapper.find(SearchItem).length).to.equal(0);
    });

    it('given two users. Should have two SearchItem', () => {
      const { enzymeWrapper } = setup({
        users: [
          {
            id: '1',
            name: 'test name 1',
            email: 'test email 1',
            createdAt: 'test created 1',
            updated: 'test updated 1',
          },
          {
            id: '2',
            name: 'test name 2',
            email: 'test email 2',
            createdAt: 'test created 2',
            updated: 'test updated 2',
          },
        ],
      });

      expect(enzymeWrapper.find(SearchItem).length).to.equal(2);
    });
  });
});
