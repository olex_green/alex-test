import React, { Component } from 'react';

import UserPage from './UserPage';

import USERS_MOCK_DATA from './usersMockData.json';

class UserPageContainer extends Component {
  constructor() {
    super();
    this.state = {
      searchText: '',
      users: USERS_MOCK_DATA,
    };
  }

  onSearchTextChange = (e) => {
    const value = e.target ? e.target.value : '';
    this.setState({
      searchText: value,
      users: USERS_MOCK_DATA.filter(user => user.name.toLowerCase().includes(value.toLowerCase())),
    });
  }

  render() {
    const {
      searchText,
      users,
    } = this.state;

    return (
      <UserPage
        searchText={searchText}
        users={users}
        onSearchTextChange={this.onSearchTextChange}
      />
    );
  }
}

export default UserPageContainer;
