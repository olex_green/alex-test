import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import AddIcon from '@material-ui/icons/Add';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import SortIcon from '@material-ui/icons/Sort';
import SettingsIcon from '@material-ui/icons/Settings';


import Header from '../../common/Header';
import LeftMenu from './LeftMenu';
import Search from './Search';

const styles = {
  root: {
    display: 'grid',
    height: 'calc(100vh - 64px)',
    gridTemplateColumns: '300px auto',
    gridTemplateRows: '64px auto',
  },
  userHeader: {
    backgroundColor: '#f5f5f5',
  },
  searchBox: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#e0e0e0',
    marginTop: 2,
    padding: 20,
  },
  addButtonBox: {
    paddingTop: 30,
    textAlign: 'right',
  },
  grow: {
    flexGrow: 1,
  },
  leftToolbarIcons: {
    color: 'black',
  },
};

function UserPage({
  classes,
  searchText,
  users,
  onSearchTextChange,
}) {
  return (
    <React.Fragment>
      <Header title="Product Name" />
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Toolbar>
            <Typography variant="h6" color="inherit" className={classes.grow}>
              Users
            </Typography>
          </Toolbar>
        </AppBar>
        <AppBar position="static" color="default">
          <Toolbar>
            <Typography variant="h6" color="inherit" className={classes.grow}>
              All Users
            </Typography>
            <div className={classes.grow} />
            <IconButton className={classes.leftToolbarIcons}>
              <ViewModuleIcon />
            </IconButton>
            <IconButton className={classes.leftToolbarIcons}>
              <SortIcon />
            </IconButton>
            <IconButton className={classes.leftToolbarIcons}>
              <SettingsIcon />
            </IconButton>
            <IconButton className={classes.leftToolbarIcons}>
              <MoreVertIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <LeftMenu />
        <div className={classes.searchBox}>
          <Search
            searchText={searchText}
            users={users}
            onSearchTextChange={onSearchTextChange}
          />
          <div className={classes.addButtonBox}>
            <Button variant="fab" color="secondary" aria-label="Add" className={classes.button}>
              <AddIcon />
            </Button>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

UserPage.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
  }),
  searchText: PropTypes.string,
  users: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    email: PropTypes.string,
    createdAt: PropTypes.string,
    updated: PropTypes.string,
  })),
  onSearchTextChange: PropTypes.func.isRequired,
};

UserPage.defaultProps = {
  classes: {},
  searchText: '',
  users: [],
};

export default withStyles(styles)(UserPage);
