import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';
import List from '@material-ui/core/List';

import SearchItem from './SearchItem';

const styles = {
  root: {
  },
  searchBox: {
    marginBottom: 16,
  },
  searchInput: {
    padding: '18px 0',
  },
  searchInputAdornment: {
    marginLeft: 20,
  },
  list: {
    padding: 0,
    overflowY: 'auto',
  },
  inputUnderline: {
    borderBottom: '1px solid #efefef',
    '&:before': {
      borderBottom: '1px solid #efefef',
    },
    '&:after': {
      borderBottom: '1px solid #efefef',
    },
  },
};

export function Search({
  classes,
  searchText,
  users,
  onSearchTextChange,
}) {
  return (
    <Paper className={classes.root}>
      <TextField
        className={classes.searchBox}
        fullWidth
        placeholder="search"
        InputProps={{
          startAdornment: (
            <InputAdornment classes={{ root: classes.searchInputAdornment }} position="start">
              <SearchIcon />
            </InputAdornment>
          ),
          classes: {
            input: classes.searchInput,
            underline: classes.inputUnderline,
          },
        }}
        value={searchText}
        onChange={onSearchTextChange}
      />
      <List className={classes.list}>
        {
          users.map(item => (
            <SearchItem {...item} />
          ))
        }
      </List>
    </Paper>
  );
}

Search.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
  }),
  searchText: PropTypes.string,
  users: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    email: PropTypes.string,
    createdAt: PropTypes.string,
    updated: PropTypes.string,
  })),
  onSearchTextChange: PropTypes.func.isRequired,
};

Search.defaultProps = {
  classes: {},
  searchText: '',
  users: [],
};

export default withStyles(styles)(Search);
