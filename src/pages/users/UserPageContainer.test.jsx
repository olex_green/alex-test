import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import UserPage from './UserPage';
import UserPageContainer from './UserPageContainer';

import USERS_MOCK_DATA from './usersMockData.json';

function setup(props) {
  const enzymeWrapper = shallow(<UserPageContainer {...props} />);

  return {
    enzymeWrapper,
  };
}

describe('UserPageContainer', () => {
  it('should render self and has first UserPage', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.first().is(UserPage)).to.equal(true);
  });

  it('by default. Should have expected initial state', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.state('searchText')).to.equal('');
    expect(enzymeWrapper.state('users')).to.deep.equal(USERS_MOCK_DATA);
  });

  it('call onSearchTextChange. Should change state as expected', () => {
    const { enzymeWrapper } = setup();
    const value = 'suz';
    const expected = [
      {
        id: '1',
        name: 'Suzy Cuningham',
        email: 'suzy.cuningham@gmail.com',
        createdAt: 'Oct 1, 2015',
        updated: '1 year ago',
      },
    ];
    enzymeWrapper.instance().onSearchTextChange({ target: { value } });
    expect(enzymeWrapper.state('searchText')).to.equal(value);
    expect(enzymeWrapper.state('users')).to.deep.equal(expected);
  });
});
