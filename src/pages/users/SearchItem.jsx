import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';
import PersonIcon from '@material-ui/icons/Person';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const styles = {
  root: {
    paddingTop: 16,
    paddingBottom: 16,
  },
  avatar: {
    margin: 10,
    color: '#707070',
    backgroundColor: '#f5f5f5',
  },
  secondaryAction: {
    display: 'flex',
    '& > div': {
      alignSelf: 'center',
    },
  },
};

function SearchItem({
  classes,
  id,
  name,
  email,
  createdAt,
  updated,
}) {
  return (
    <React.Fragment>
      <ListItem key={id} className={classes.root}>
        <Avatar className={classes.avatar}>
          <PersonIcon />
        </Avatar>
        <ListItemText primary={name} secondary={email} />
        <ListItemSecondaryAction className={classes.secondaryAction}>
          <ListItemText secondary={createdAt} />
          <ListItemText secondary={updated} />
          <IconButton>
            <MoreVertIcon />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
      <Divider inset component="li" />
    </React.Fragment>
  );
}

SearchItem.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
  }),
  id: PropTypes.string,
  name: PropTypes.string,
  email: PropTypes.string,
  createdAt: PropTypes.string,
  updated: PropTypes.string,
};

SearchItem.defaultProps = {
  classes: {},
  id: '',
  name: '',
  email: '',
  createdAt: '',
  updated: '',
};

export default withStyles(styles)(SearchItem);
