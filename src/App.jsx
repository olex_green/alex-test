import React from 'react';
import { hot } from 'react-hot-loader';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import UserPageContainer from './pages/users/UserPageContainer';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#1976d2',
    },
    secondary: {
      main: '#ef6c00',
    },
  },
});

function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      <UserPageContainer />
    </MuiThemeProvider>
  );
}

export default hot(module)(App);
